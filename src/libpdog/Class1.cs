﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace jas
{
    public class libpdog
    {
        public void Generate()
        {
            Image bmp = new Bitmap(500, 500);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;

                Rectangle r = new Rectangle(0, 0, 500, 500);
                Rectangle b = new Rectangle(10, 10, 480, 480);

                using (Pen pen = new Pen(Color.Blue, 5))
                {
                    g.FillRectangle(Brushes.White, r);
                    g.DrawRectangle(pen, b);
                }

                StringFormat stringFormat = new StringFormat()
                {
                    Alignment = StringAlignment.Center
                };

                g.DrawString("QA Pharmacy", new Font("Segoe UI", 18), Brushes.Blue, b, stringFormat);
                g.Flush();
            }

            bmp.Save(@"TestBitmap.bmp");
        }
    }
}