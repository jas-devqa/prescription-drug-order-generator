﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jas
{
    internal class PrescriptionDrugOrder
    {
        uint Number { get; set; }
        string Patient { get; set; }
        string Pharmacy { get; set; }
        DateTime Date { get; set; }
        string Drug { get; set; }
        string DrugStrength { get; set; }
        string DrugForm { get; set; }
        string Directions { get; set; }
        uint DispenseQuantity { get; set; }
        string Signature { get; set; }
        string DeaNumber { get; set; }
        string PrescriberName { get; set; }
        string PrescriberAddressLine1 { get; set; }
        string PrescriberAddressLine2 { get; set; }
        string PrescriberAddressCity { get; set; }
        string PrescriberAddressState { get; set; }
        string PrescriberAddressZipCode { get; set; }
        string PrescriberTelephone { get; set; }
    }
}
